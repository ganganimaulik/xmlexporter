<?php

namespace App\Modules;



use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DataTables
{
    static public function handleAllProducts(User $user)
    {
        $datatable = ! empty( $_REQUEST[ 'datatable' ] ) ? $_REQUEST[ 'datatable' ] : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        // search filter by keywords
        $filter = isset( $datatable[ 'query' ][ 'q' ] ) && is_string( $datatable[ 'query' ][ 'q' ] ) ? $datatable[ 'query' ][ 'q' ] : '';

        $products = $user->products();

        $total = $products->count();

        if ( ! empty( $filter ) ) {
            $filter = '%' . $filter . '%';
            $products->where(function ($query) use ($filter) {
               $query->where('title', 'like', $filter)
                   ->orWhere('shopify_pid', 'like', $filter)
                   ->orWhere('status', 'like', $filter);
            });
        }

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'asc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'shopify_pid';

        $products->orderBy($field, $sort);


        $meta    = array();
        $pages = 1;
        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;


        if($perpage > 0) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

            $products->offset($offset)->limit($perpage);
        }

        $products = $products->select('shopify_pid', 'title', 'xml_conf', 'ftp_type', 'ftp_host', 'ftp_user', 'ftp_password', 'ftp_folder', 'status')
        ->get()->toArray();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $products
        );

        return $result;
    }

    static public function handleEnabledProducts(User $user)
    {
        $datatable = ! empty( $_REQUEST[ 'datatable' ] ) ? $_REQUEST[ 'datatable' ] : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        // search filter by keywords
        $filter = isset( $datatable[ 'query' ][ 'q' ] ) && is_string( $datatable[ 'query' ][ 'q' ] ) ? $datatable[ 'query' ][ 'q' ] : '';

        $products = $user->products()->where('status', 'enabled');

        $total = $products->count();

        if ( ! empty( $filter ) ) {
            $filter = '%' . $filter . '%';

            $products->where(function ($query) use ($filter) {
                $query->where('title', 'like', $filter)
                    ->orWhere('shopify_pid', 'like', $filter)
                    ->orWhere('status', 'like', $filter);
            });
        }

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'asc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'shopify_pid';

        $products->orderBy($field, $sort);



        $meta    = array();
        $pages = 1;
        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;


        if($perpage > 0) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

            $products->offset($offset)->limit($perpage);
        }

        $products = $products->select('shopify_pid', 'title', 'xml_conf', 'ftp_type', 'ftp_host', 'ftp_user', 'ftp_password', 'ftp_folder')
            ->get()->toArray();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $products
        );

        return $result;
    }

    static public function handleDisabledProducts(User $user)
    {
        $datatable = ! empty( $_REQUEST[ 'datatable' ] ) ? $_REQUEST[ 'datatable' ] : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        // search filter by keywords
        $filter = isset( $datatable[ 'query' ][ 'q' ] ) && is_string( $datatable[ 'query' ][ 'q' ] ) ? $datatable[ 'query' ][ 'q' ] : '';

        $products = $user->products()->where('status', 'disabled');

        $total = $products->count();

        if ( ! empty( $filter ) ) {
            $filter = '%' . $filter . '%';
            $products->where(function ($query) use ($filter) {
                $query->where('title', 'like', $filter)
                    ->orWhere('shopify_pid', 'like', $filter)
                    ->orWhere('status', 'like', $filter);
            });
        }

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'asc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'shopify_pid';

        $products->orderBy($field, $sort);



        $meta    = array();
        $pages = 1;
        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;


        if($perpage > 0) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

            $products->offset($offset)->limit($perpage);
        }

        $products = $products->select('shopify_pid', 'title', 'xml_conf', 'ftp_type', 'ftp_host', 'ftp_user', 'ftp_password', 'ftp_folder')
            ->get()->toArray();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $products
        );

        return $result;
    }
}