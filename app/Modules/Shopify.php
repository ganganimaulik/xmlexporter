<?php

namespace App\Modules;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;


class Shopify
{
    private $store_name, $api_key, $api_password, $shared_secret, $base_url, $last_call;

    public function __construct($store_name, $api_key, $api_password, $shared_secret = null)
    {
        $this->store_name = $store_name;
        $this->api_key = $api_key;
        $this->api_password = $api_password;
        $this->shared_secret = $shared_secret;
        $this->base_url = 'https://' . $this->api_key . ':' . $this->api_password . '@' . $this->store_name . '.myshopify.com/admin/';
    }

    public function handleThrottle()
    {
        $key = 'api_count:' . time();
        if (Cache::has($key)) {
            Cache::increment($key);

            $sleeped = false;
            while (Cache::get($key) > 2) {
                sleep(1);
                $sleeped = true;
                $key = 'api_count:' . time();
            }

            $sleeped ? Cache::put($key, 1, 1) : null;
        } else {
            Cache::put($key, 1, 1);
        }
    }

    public function testCredentials()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);


        $this->handleThrottle();
        $response = $client->request('GET', 'shop.json');

        return \GuzzleHttp\json_decode($response->getBody());
    }

    public function fetchProducts()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);

        $this->handleThrottle();
        $response = $client->request('GET', 'products/count.json');
        $count = \GuzzleHttp\json_decode($response->getBody())->count;

        $pages = ceil($count / 250);

        $result = [];
        for ($i = 1; $i <= $pages; $i++) {
            $this->handleThrottle();
            $response = $client->request('GET', 'products.json?limit=250&page=' . $i);
            $response = \GuzzleHttp\json_decode($response->getBody());
            $result = array_merge((array)$result, (array)$response);
        }

        return $result['products'];
    }

    public function fetchWebhooks()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);


        $this->handleThrottle();
        $response = $client->request('GET', 'webhooks/count.json');
        $count = \GuzzleHttp\json_decode($response->getBody())->count;

        if ($count == 0) {
            return null;
        } else {
            $pages = ceil($count / 250);

            $result = [];
            for ($i = 1; $i <= $pages; $i++) {
                $this->handleThrottle();
                $response = $client->request('GET', 'webhooks.json?limit=250&page=' . $i);
                $response = \GuzzleHttp\json_decode($response->getBody());
                $result = array_merge((array)$result, (array)$response);
            }

            return $result['webhooks'];
        }
    }

    public function createWebhook($topic, $url)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);


        $this->handleThrottle();

        $options = [
            "webhook" => [
                "topic" => $topic,
                "address" => $url,
                "format" => "json"
            ]
        ];


        $response = $client->request('POST', 'webhooks.json', [RequestOptions::JSON => $options]);

        return $response;
    }

    public function deleteWebhook($id)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);


        $this->handleThrottle();

        $response = $client->request('DELETE', 'webhooks/' . $id . '.json');

        return \GuzzleHttp\json_decode($response->getBody());
    }

    public function getOrder($id)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->base_url,
            // You can set any number of default request options.
            'timeout' => 15.0,
        ]);

        $this->handleThrottle();

        $response = $client->request('GET', '/admin/orders/' . $id . '.json');

        return \GuzzleHttp\json_decode($response->getBody());
    }

    public function verifyWebhook()
    {
        $data = file_get_contents('php://input');
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

        if (empty($hmac_header)) {
            // Header should not be empty.
            throw new \Exception('HMAC Header is empty.');
        }
        if (empty($data)) {
            // Data should not be empty.
            throw new \Exception('Data is empty');
        }

        if ($hmac_header !== $this->calculateHmac($data, $this->shared_secret)) {
            // Data or the hash are corrupt.
            throw new \Exception('Invalid webhook');
        }

        return ($webhook_is_valid = TRUE);
    }


    /**
     * Calculates the HMAC based on Shopify's specification.
     *
     * @param string $data
     *   JSON data.
     * @param string $secret
     *   Shopify shared secret.
     *
     * @return string
     */
    public function calculateHmac($data, $secret)
    {
        return base64_encode(hash_hmac('sha256', $data, $secret, TRUE));
    }

}