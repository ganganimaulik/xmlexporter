<?php

namespace App\Jobs;

use App\Modules\Shopify;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SyncProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // fetch all products from shopify
        $credentials = $this->user->api_credential;
        $shopify = new Shopify($credentials->store_name, $credentials->api_key, $credentials->api_password, $credentials->shared_secret);
        $api_products = $shopify->fetchProducts();

        // fetch all products from database
        $db_products = $this->user->products;

        $now = Carbon::now('utc')->toDateTimeString();
        $insert_array = [];

        if ($db_products != null) {
            // merge both
            foreach ($api_products as $api_product) {
                $db_product = $db_products->where('shopify_pid',$api_product->id)->first();

                if ($db_product == null) {
                    // this api_product doesn't exist in db... so insert
                    $insert_array[] = [
                        'user_id' => $this->user->id,
                        'shopify_pid' => $api_product->id,
                        'title' => $api_product->title,
                        'status' => 'disabled',
                        'product_json' => \GuzzleHttp\json_encode($api_product),
                        'created_at' => $now,
                        'updated_at' => $now
                    ];
                } else {
                    // product exist in database... so update it
                    $db_product->title = $api_product->title;
                    $db_product->product_json = \GuzzleHttp\json_encode($api_product);
                    $db_product->save();
                }
            }
        } else {
            // no products in db, insert all
            foreach ($api_products as $api_product) {
                $insert_array[] = [
                    'user_id' => $this->user->id,
                    'shopify_pid' => $api_product->id,
                    'title' => $api_product->title,
                    'status' => 'disabled',
                    'product_json' => \GuzzleHttp\json_encode($api_product),
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }


        Product::insert($insert_array);

        Log::info("sync for user: " . $this->user->name);
    }
}
