<?php

namespace App\Jobs;

use App\Mail\OrderProductXml;
use App\Product;
use App\User;
use FtpClient\FtpClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use phpseclib\Net\SFTP;

class SendXmlForProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    protected $user, $replacements;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $replacements)
    {
        $this->user = $user;
        $this->replacements = $replacements;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product = Product::where('shopify_pid', $this->replacements['product->product_id'])->first();
        $xml = $product->xml_conf;

        foreach ($this->replacements as $key => $value) {
            $key = '{{' . $key . '}}';
            $xml = str_replace($key, $value, $xml);
        }

        if ($this->user->send_emails == true) {
            Mail::to($this->user)->send(new OrderProductXml($xml, $this->replacements['order->order_number']));
        }

        $xml_file = 'orders/' . $this->replacements['order->id'] . '.xml';
        Storage::disk('local')->put($xml_file, $xml);

        if ($product->ftp_type === 'ftp') {
            try {
                $ftp = new FtpClient();
                $ftp->connect($product->ftp_host, false, 21, 45);
                $ftp->login($product->ftp_user, $product->ftp_password);

                $ftp->put($product->ftp_folder . $this->replacements['order->id'] . '.xml', storage_path('app/' . $xml_file), FTP_ASCII);
            } catch (\Exception $e) {
                Log::critical("cant login/upload file to ftp for orderid #" . $this->replacements['order->id']);
            }
        } else {
            $sftp = new SFTP($product->ftp_host);
            if (!$sftp->login($product->ftp_user, $product->ftp_password)) {
                Log::critical("cant login/upload file to ftp for orderid #" . $this->replacements['order->id']);
            }

            $sftp->put($product->ftp_folder . $this->replacements['order->id'] . '.xml', storage_path('app/' . $xml_file), SFTP::SOURCE_LOCAL_FILE);
            try {
                $sftp = new SFTP($product->ftp_host);
                if (!$sftp->login($product->ftp_user, $product->ftp_password)) {
                    Log::critical("cant login/upload file to ftp for orderid #" . $this->replacements['order->id']);
                }

                $sftp->put($this->replacements['order->id'] . '.xml', storage_path('app/' . $xml_file),SFTP::SOURCE_LOCAL_FILE );
            } catch (\Exception $e) {
                Log::critical("cant login/upload file to ftp for orderid #" . $this->replacements['order->id']);

            }
        }

    }
}
