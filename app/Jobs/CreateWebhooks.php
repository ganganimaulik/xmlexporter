<?php

namespace App\Jobs;

use App\Modules\Shopify;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class CreateWebhooks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // fetch all WEBHOOKS from shopify
        $credentials = $this->user->api_credential;
        $shopify = new Shopify($credentials->store_name, $credentials->api_key, $credentials->api_password, $credentials->shared_secret);
        $api_webhooks = $shopify->fetchWebhooks();


        $tobe_created = ['products/create', 'products/delete', 'products/update', 'orders/paid'];

        // remove webhooks which are already created
        if ($api_webhooks != null) {
            $tobe_removed = [];

            foreach ($api_webhooks as $api_webhook) {
                $url = config('app.url') . '/webhooks/' . $api_webhook->topic . '/' . $this->user->id;
                if(in_array($api_webhook->topic, $tobe_created) && $url === $api_webhook->address) {
                    $tobe_removed[] = $api_webhook->topic;
                }
            }

            $tobe_created = array_diff($tobe_created, $tobe_removed);
        }

        // create webhooks which are not already created
        if (sizeof($tobe_created) > 0) {
            foreach ($tobe_created as $topic) {
                $url = config('app.url') . '/webhooks/' . $topic . '/' . $this->user->id;
                $response = $shopify->createWebhook($topic, $url);

                if($response->getStatusCode() != 201) {
                    Log::info(print_r($response->getBody(), true));
                }
            }
        }
    }
}
