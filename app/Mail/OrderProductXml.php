<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderProductXml extends Mailable
{
    use Queueable, SerializesModels;

    public $xml, $order_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($xml, $order_id)
    {
        $this->xml = $xml;
        $this->order_id = $order_id;
        $this->subject = "XML for order: #" . $order_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.OrderProductXml');
    }
}
