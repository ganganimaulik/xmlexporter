<?php

/*
 * Returns " active open" for main menu in sidebar.
 */
function set_active_class($path, $class='m-menu__item--active')
{

    if (Request::is($path . '*')) {
        return ' ' . $class;
    }

    return '';
}
