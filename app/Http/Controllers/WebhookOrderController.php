<?php

namespace App\Http\Controllers;

use App\Jobs\SendXmlForProduct;
use App\Modules\Shopify;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WebhookOrderController extends Controller
{
    public function paid($id)
    {
        $user = $this->verifyRequest($id);
        $order = json_decode(file_get_contents('php://input'));

        Log::info(print_r($order, true));



        $char = 'A';

        $frieght_codes = [
            //Domestic Freight Codes (Fastest to slowest)
            [
                'carrier_code' => 'U01',
                'carrier_description' => 'Next Day Air',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U43',
                'carrier_description' => 'Next Day Air Saver',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U07',
                'carrier_description' => '2nd Day Air',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U21',
                'carrier_description' => '3 Day Select',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U11',
                'carrier_description' => 'Ground',
                'carrier' => 'UPS'
            ],

            //International Freight Codes (Fastest to slowest)
            [
                'carrier_code' => 'U63',
                'carrier_description' => 'Worldwide Express Plus (Exp Plus Svc Intl)',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U49',
                'carrier_description' => 'Worldwide Express (Exp Intl)',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U98',
                'carrier_description' => 'Worldwide Saver',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'U54',
                'carrier_description' => 'Worldwide Expedited (Exp Svc Intl)',
                'carrier' => 'UPS'
            ],

            //USPS Freight Codes
            [
                'carrier_code' => 'P01',
                'carrier_description' => 'First Class Regular',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'P03',
                'carrier_description' => 'Priority Mail',
                'carrier' => 'UPS'
            ],
            [
                'carrier_code' => 'P85',
                'carrier_description' => 'USPS International Priority Mail',
                'carrier' => 'UPS'
            ]

        ];

        $order_frieght_code = null;
        if (isset($order->shipping_lines[0]->title)) {
            foreach ($frieght_codes as $frieght_code) {
                if(strtolower($frieght_code['carrier_description'] . ' [ASR]') === strtolower($order->shipping_lines[0]->title)
                || strtolower($frieght_code['carrier_description']) === strtolower($order->shipping_lines[0]->title)) {
                    $order_frieght_code =  $frieght_code;
                }
            }
        }

        if ($order_frieght_code == null) {
            $order_frieght_code = [
                'carrier_code' => '',
                'carrier_description' => '',
                'carrier' => ''
            ];
        }

        foreach ($order->line_items as $product) {
            $db_product  = Product::where('shopify_pid', $product->product_id)->first();
            if ($db_product->status ==='enabled') {
                $replacements = [
                    // order specific variables
                    'order->id' => count($order->line_items) > 1 ? $order->id . ' - ' . $char++ : $order->id,
                    'order->email' => $order->email,
                    'order->created_at' => $order->created_at,
                    'order->updated_at' => $order->updated_at,
                    'order->number' => $order->number,
                    'order->gateway' => $order->gateway,
                    'order->total_price' => $order->total_price,
                    'order->subtotal_price' => $order->subtotal_price,
                    'order->total_weight' => $order->total_weight,
                    'order->total_tax' => $order->total_tax,
                    'order->currency' => $order->currency,
                    'order->total_discounts' => $order->total_discounts,
                    'order->total_line_items_price' => $order->total_line_items_price,
                    'order->name' => $order->name,
                    'order->total_price_usd' => $order->total_price_usd,
                    'order->processed_at' => $order->processed_at,
                    'order->customer_locale' => $order->customer_locale,
                    'order->order_number' => $order->order_number,
                    'order->processing_method' => $order->processing_method,
                    'order->contact_email' => $order->contact_email,
                    'order->order_status_url' => $order->order_status_url,
                    'order->source_name' => $order->source_name,

                    'order->billing_address->name' => $order->billing_address->name,
                    'order->billing_address->first_name' => $order->billing_address->first_name,
                    'order->billing_address->last_name' => $order->billing_address->last_name,
                    'order->billing_address->address1' => $order->billing_address->address1,
                    'order->billing_address->address2' => $order->billing_address->address2,
                    'order->billing_address->phone' => $order->billing_address->phone,
                    'order->billing_address->city' => $order->billing_address->city,
                    'order->billing_address->zip' => $order->billing_address->zip,
                    'order->billing_address->province' => $order->billing_address->province,
                    'order->billing_address->country' => $order->billing_address->country,
                    'order->billing_address->company' => $order->billing_address->company,
                    'order->billing_address->latitude' => $order->billing_address->latitude,
                    'order->billing_address->longitude' => $order->billing_address->longitude,
                    'order->billing_address->country_code' => $order->billing_address->country_code,
                    'order->billing_address->province_code' => $order->billing_address->province_code,

                    'order->shipping_address->name' => $order->shipping_address->name,
                    'order->shipping_address->first_name' => $order->shipping_address->first_name,
                    'order->shipping_address->last_name' => $order->shipping_address->last_name,
                    'order->shipping_address->address1' => $order->shipping_address->address1,
                    'order->shipping_address->address2' => $order->shipping_address->address2,
                    'order->shipping_address->phone' => $order->shipping_address->phone,
                    'order->shipping_address->city' => $order->shipping_address->city,
                    'order->shipping_address->zip' => $order->shipping_address->zip,
                    'order->shipping_address->province' => $order->shipping_address->province,
                    'order->shipping_address->country' => $order->shipping_address->country,
                    'order->shipping_address->company' => $order->shipping_address->company,
                    'order->shipping_address->latitude' => $order->shipping_address->latitude,
                    'order->shipping_address->longitude' => $order->shipping_address->longitude,
                    'order->shipping_address->country_code' => $order->shipping_address->country_code,
                    'order->shipping_address->province_code' => $order->shipping_address->province_code,

                    'order->shipping->carrier_code' => $order_frieght_code['carrier_code'],
                    'order->shipping->carrier_description' => $order_frieght_code['carrier_description'],
                    'order->shipping->carrier' => $order_frieght_code['carrier'],


                    // product specific variables
                    'product->id' => $product->id,
                    'product->variant_id' => $product->variant_id,
                    'product->title' => $product->title,
                    'product->quantity' => $product->quantity,
                    'product->price' => $product->price,
                    'product->sku' => $product->sku,
                    'product->variant_title' => $product->variant_title,
                    'product->vendor' => $product->vendor,
                    'product->fulfillment_service' => $product->fulfillment_service,
                    'product->product_id' => $product->product_id,
                    'product->name' => $product->name,
                    'product->variant_inventory_management' => $product->variant_inventory_management,
                    'product->fulfillable_quantity' => $product->fulfillable_quantity,
                    'product->grams' => $product->grams,
                    'product->total_discount' => $product->total_discount,

                    'product->origin_location->country_code' => $product->origin_location->country_code,
                    'product->origin_location->province_code' => $product->origin_location->province_code,
                    'product->origin_location->name' => $product->origin_location->name,
                    'product->origin_location->address1' => $product->origin_location->address1,
                    'product->origin_location->address2' => $product->origin_location->address2,
                    'product->origin_location->city' => $product->origin_location->city,
                    'product->origin_location->zip' => $product->origin_location->zip,

                    'product->destination_location->country_code' => $product->destination_location->country_code,
                    'product->destination_location->province_code' => $product->destination_location->province_code,
                    'product->destination_location->name' => $product->destination_location->name,
                    'product->destination_location->address1' => $product->destination_location->address1,
                    'product->destination_location->address2' => $product->destination_location->address2,
                    'product->destination_location->city' => $product->destination_location->city,
                    'product->destination_location->zip' => $product->destination_location->zip,
                ];
                $array_file = 'orders/' . $replacements['order->id'] . '.array';
                Storage::disk('local')->put($array_file, print_r($order, true));

                SendXmlForProduct::dispatch($user, $replacements)->onQueue('high');
            }
        }

        return response('thanks');
    }

    public function verifyRequest($id)
    {
        try {
            $user = User::find($id);
            $credentials = $user->api_credential;
            $shopify = new Shopify($credentials->store_name, $credentials->api_key, $credentials->api_password, $credentials->shared_secret);
            $shopify->verifyWebhook();
        } catch (\Exception $e) {
            exit(200);
        }

        return $user;
    }
}
