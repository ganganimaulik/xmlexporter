<?php

namespace App\Http\Controllers;

use App\Modules\Shopify;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookProductController extends Controller
{
    public function create($id)
    {
        $this->verifyRequest($id);

        $data = json_decode(file_get_contents('php://input'));

        $product = new Product;
        $product->title = $data->title;
        $product->shopify_pid = $data->id;
        $product->user_id = $id;
        $product->status = 'disabled';
        $product->product_json = \GuzzleHttp\json_encode($data);
        $product->save();

        return response('thanks');
    }

    public function delete($id)
    {
        $user = $this->verifyRequest($id);

        $data = json_decode(file_get_contents('php://input'));

        $product = $user->products()->where('shopify_pid', $data->id)->first();
        $product->delete();

        return response('thanks');
    }

    public function update($id)
    {
        $user = $this->verifyRequest($id);

        $data = json_decode(file_get_contents('php://input'));


        $product = $user->products()->where('shopify_pid', $data->id)->first();
        $product->title = $data->title;
        $product->product_json = \GuzzleHttp\json_encode($data);
        $product->save();

        return response('thanks');
    }

    public function verifyRequest($id)
    {
        try {
            $user = User::find($id);
            $credentials = $user->api_credential;
            $shopify = new Shopify($credentials->store_name, $credentials->api_key, $credentials->api_password, $credentials->shared_secret);
            $shopify->verifyWebhook();
        } catch (\Exception $e) {
            exit(200);
        }

        return $user;
    }
}
