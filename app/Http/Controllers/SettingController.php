<?php

namespace App\Http\Controllers;

use App\ApiCredential;
use App\Jobs\CreateWebhooks;
use App\Jobs\SyncProducts;
use App\Rules\Timezone;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    public function show()
    {
        return view('settings');
    }

    public function storeApi(Request $request)
    {
        $validatedData = $request->validate([
            'store_name' => 'required|unique:api_credentials,store_name,' . Auth::user()->id . ',user_id',
            'api_key' => 'required',
            'api_password' => 'required',
            'shared_secret' => 'required'
        ]);

        $api_credentials = Auth::user()->api_credential;

        // verify api credentials with shopify
        try {
            $shopify = new \App\Modules\Shopify($request->input('store_name'), $request->input('api_key')
                , $request->input('api_password'), $request->input('shared_secret'));
            $shopify->testCredentials();
        } catch (ClientException $e) {
            return response('invalid keys');
        }


        if($api_credentials != null) {
            if($api_credentials->store_name != $request->input('store_name')) {
                //store name is changed so remove all products
                Auth::user()->products()->delete();
            }
        } else {
            // create
            $api_credentials = new ApiCredential;

            // fire job to sync products
            SyncProducts::dispatch(Auth::user())->onQueue('high')
                ->delay(Carbon::now()->addSeconds(5));
        }

        CreateWebhooks::dispatch(\Illuminate\Support\Facades\Auth::user())->onQueue('high');

        $api_credentials->user_id = Auth::user()->id;
        $api_credentials->store_name = $request->input('store_name');
        $api_credentials->api_key = $request->input('api_key');
        $api_credentials->api_password = $request->input('api_password');
        $api_credentials->shared_secret = $request->input('shared_secret');
        $api_credentials->is_valid = true;
        $api_credentials->save();

        return response('success');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'oldpassword' => 'required',
            'newpassword' => 'required|confirmed|min:6',
        ]);

        $current_password = Auth::User()->password;
        if(Hash::check($request->input('oldpassword'), $current_password)) {
            $user = Auth::user();
            $user->password = Hash::make($request->input('newpassword'));
            $user->save();
            return response('success');
        } else {
            return response('invalid oldpassword');
        }
    }

    public function otherSettings(Request $request)
    {
        $this->validate($request, [
            'timezone' => ['required', new Timezone]
        ]);

        $user = Auth::user();
        $user->timezone = $request->input('timezone');
        $user->send_emails = $request->input('email_notifications') === 'on' ? true : false;
        $user->save();

        return response('success');
    }

    public function dtShipingCodes()
    {

    }
}
