<?php

namespace App\Http\Controllers;

use App\Modules\DataTables;
use App\Product;
use FtpClient\FtpClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use phpseclib\Net\SFTP;

class ProductController extends Controller
{
    public function all()
    {
        return view('products.all');
    }

    public function enabled()
    {
        return view('products.enabled');
    }

    public function disabled()
    {
        return view('products.disabled');
    }

    public function dtAll()
    {
        return DataTables::handleAllProducts(Auth::user());
    }

    public function dtEnabled()
    {
        return DataTables::handleEnabledProducts(Auth::user());

    }

    public function dtDisabled()
    {
        return DataTables::handleDisabledProducts(Auth::user());
    }

    public function storeFtp($shopify_pid, Request $request)
    {
        $validatedData = $request->validate([
            'ftp_host' => 'required',
            'ftp_user' => 'required',
            'ftp_password' => 'required',
            'ftp_type' => 'required'
        ]);

        if($request->input('ftp_type') === 'ftp') {
            try {
                $ftp = new FtpClient();
                $ftp->connect($request->input('ftp_host'), false, 21, 5);
                $ftp->login($request->input('ftp_user'), $request->input('ftp_password'));
            } catch (\Exception $e) {
                return $this->sendInvalidFtpResponse();
            }
        } else {
            try {
                $sftp = new SFTP($request->input('ftp_host'));
                if (!$sftp->login($request->input('ftp_user'), $request->input('ftp_password'))) {
                    return $this->sendInvalidFtpResponse();
                }
            } catch (\Exception $e) {
                return $this->sendInvalidFtpResponse();
            }
        }

        $product = Product::where('shopify_pid', $shopify_pid)->first();
        $product->ftp_host = $request->input('ftp_host');
        $product->ftp_type = $request->input('ftp_type');
        $product->ftp_user = $request->input('ftp_user');
        $product->ftp_folder = rtrim($request->input('ftp_folder'), '/') . '/';
        $product->ftp_password = $request->input('ftp_password');
        $product->save();

        return response('success');
    }

    private function sendInvalidFtpResponse() {
        $invalidCredentialsResponse = [
            'errors' => [
                'ftp_host' => ['Invalid ftp credentials.']
            ]
        ];
        return response()->json($invalidCredentialsResponse, 422);
    }

    public function storeXml($shopify_pid, Request $request)
    {
        $validatedData = $request->validate([
            'xml_conf' => 'required'
        ]);

        if (!$this->is_valid_xml($request->input('xml_conf'))) {
            $invalidXmlResponse = [
                'errors' => [
                    'xml_conf' => ['Invalid xml provided.']
                ]
            ];
            return response()->json($invalidXmlResponse, 422);
        }

        $product = Product::where('shopify_pid', $shopify_pid)->first();
        $product->xml_conf = $request->input('xml_conf');
        $product->save();

        return response('success');
    }


    public function enable($shopify_pid)
    {
        $product = Product::where('shopify_pid', $shopify_pid)->first();
        if(Auth::user()->id != $product->user_id) {
            exit(403);
        }
        if(!empty($product->xml_conf) && !empty($product->ftp_host)) {
            $product->status = 'enabled';
            $product->save();

            return response('success');
        } else {
            return response('fail');
        }
    }

    public function disable($shopify_pid)
    {
        $product = Product::where('shopify_pid', $shopify_pid)->first();
        if(Auth::user()->id != $product->user_id) {
            exit(403);
        }

        $product->status = 'disabled';
        $product->save();

        return response('success');
    }



    /**
     *  Takes XML string and returns a boolean result where valid XML returns true
     */
    public function is_valid_xml ( $xml ) {
        libxml_use_internal_errors( true );

        $doc = new \DOMDocument('1.0', 'utf-8');

        $doc->loadXML( $xml );

        $errors = libxml_get_errors();

        return empty( $errors );
    }
}
