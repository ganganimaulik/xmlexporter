<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNoApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_credential = Auth::user()->api_credential;
        if($api_credential == null) {
            return redirect('/settings');
        } elseif ($api_credential->is_valid == false) {
            return redirect('/settings');
        } else {
            return $next($request);
        }
    }
}
