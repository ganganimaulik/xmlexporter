let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets', 'public/assets')
    .styles(['resources/assets/vendors/base/vendors.bundle.css'], 'public/assets/vendors/base/vendors.bundle.css')
    .styles(['resources/assets/demo/demo6/base/style.bundle.css'], 'public/assets/demo/demo6/base/style.bundle.css')
    .styles(['resources/assets/exporter/css/custom.css'], 'public/assets/exporter/css/custom.css')
    .scripts(['resources/assets/vendors/base/vendors.bundle.js'], 'public/assets/vendors/base/vendors.bundle.js')
    .styles(['resources/assets/demo/default/base/style.bundle.css'], 'public/assets/demo/default/base/style.bundle.css')
    .scripts(['resources/assets/demo/default/base/scripts.bundle.js'], 'public/assets/demo/default/base/scripts.bundle.js')
    .scripts(['resources/assets/app/js/dashboard.js'], 'public/assets/app/js/dashboard.js')
    .scripts(['resources/assets/app/js/enabled.js'], 'public/assets/app/js/enabled.js')
    .scripts(['resources/assets/app/js/disabled.js'], 'public/assets/app/js/disabled.js')
    .scripts(['resources/assets/exporter/js/settings.js'], 'public/assets/exporter/js/settings.js');
