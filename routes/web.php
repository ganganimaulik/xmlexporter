<?php

// Auth Routes
use App\Modules\Shopify;
use FtpClient\FtpClient;

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('register', 'Auth\RegisterController@register');


// Routes for list product pages
Route::group(['middleware' => ['auth', 'api-valid']], function () {
    // views
    Route::get('/products/all', 'ProductController@all')->name('products.all');
    Route::get('/products/enabled', 'ProductController@enabled')->name('products.enabled');
    Route::get('/products/disabled', 'ProductController@disabled')->name('products.disabled');

    // ajax datatables
    Route::post('/products/all', 'ProductController@dtAll');
    Route::post('/products/enabled', 'ProductController@dtEnabled');
    Route::post('/products/disabled', 'ProductController@dtDisabled');

    Route::post('/products/{shopify_pid}/ftp', 'ProductController@storeFtp');
    Route::post('/products/{shopify_pid}/xml', 'ProductController@storeXml');
    Route::post('/products/{shopify_pid}/enable', 'ProductController@enable');
    Route::post('/products/{shopify_pid}/disable', 'ProductController@disable');


});


Route::group(['middleware' => ['auth']], function () {
    Route::get('/settings', 'SettingController@show')->name('settings');

    Route::post('/settings/api', 'SettingController@storeApi');
    Route::post('/settings/change-password', 'SettingController@changePassword');
    Route::post('/settings/other-settings', 'SettingController@otherSettings');

});

Route::prefix('webhooks')->group(function () {
    Route::post('products/create/{id}', 'WebhookProductController@create');
    Route::post('products/delete/{id}', 'WebhookProductController@delete');
    Route::post('products/update/{id}', 'WebhookProductController@update');

    Route::post('orders/paid/{id}', 'WebhookOrderController@paid');
});

Route::get('/test', function () {

    print_r(ftp_ssl_connect('pixahex.com', '22', '5') or die('cannot c'));
});