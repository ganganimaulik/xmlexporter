//== Class Definition
var SnippetSettings = function() {

    var showErrorMsg = function(type, msg) {
        var content = {};

        content.message = msg;

        var notify = $.notify(content, {
            type: type,
            allow_dismiss: true,
            newest_on_top: false,
            mouse_over:  false,
            showProgressbar:  false,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animated bounce',
                exit: 'animated bounce'
            }
        });
    }


    var initSelectTimezone = function () {
        var selectTimezone = $('#timezone');
        window.selectTimezone = selectTimezone;
        selectTimezone.select2({
            placeholder: "Select a timezone",
            width: '400px'
        });


        var timezone = $.trim($('#user_timezone').text());
        selectTimezone.val(timezone).trigger("change");
    }

    var cleanForm = function (form , clear) {
        clear ? form.clearForm() : form.trigger("reset");
        form.validate().resetForm();

        form.find('.has-error').removeClass("has-error");
        form.find('.has-success').removeClass("has-success");
        form.find('.form-control-feedback').remove()

    };

    var handleShopifyConfig = function() {
        $('#save_shopify_config').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    store_name: {
                        required: true,
                    },
                    api_key: {
                        required: true
                    },
                    api_password: {
                        required: true
                    },
                    shared_secret: {
                        required: true
                    },

                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '/settings/api',
                success: function(response, status, xhr, $form) {
                    console.log(response);

                    if(response==='success') {
                        showErrorMsg('success', 'API keys validated. Redirecting...');
                        window.location.replace('/products/all');
                    } else if(response==='invalid keys') {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        cleanForm(form, false);
                        showErrorMsg('danger', 'Invalid API keys. Try again.');
                    } else {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        cleanForm(form, false);
                        showErrorMsg('danger', 'Something went wrong!');
                    }
                },
                error: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);
                    showErrorMsg('danger', 'Something went wrong!');
                }
            });
        });
    }


    var handleChangePassword = function () {
        $('#change_password').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    oldpassword: {
                        required: true,
                    },
                    newpassword: {
                        required: true,
                        minlength: 6
                    },
                    newpassword_confirmation: {
                        equalTo: '#newpassword',
                        minlength: 6,
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }


            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '/settings/change-password',
                success: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);
                    if(response==='success') {
                        showErrorMsg('success', 'Password has been changed.');
                    } else if(response==='invalid oldpassword') {
                        showErrorMsg('danger', 'Invalid Old Password. Try again.');
                    } else {
                        showErrorMsg('danger', 'Something went wrong!');
                    }
                },
                error: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);
                    showErrorMsg('danger', 'Something went wrong!');
                }
            });
        });
    }

    var handleOtherSettings = function () {
        $('#other_settings').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');


            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '/settings/other-settings',
                success: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if(response==='success') {
                        showErrorMsg('success', 'Settings has been updated.');
                    } else {
                        cleanForm(form, false);
                        showErrorMsg('danger', 'Something went wrong!');
                    }
                },
                error: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);
                    showErrorMsg('danger', 'Something went wrong!');
                }
            });
        });
    }

    //== Public Functions
    return {
        // public functions
        init: function() {
            initSelectTimezone();
            handleShopifyConfig();
            handleChangePassword();
            handleOtherSettings();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function() {
    SnippetSettings.init();
});