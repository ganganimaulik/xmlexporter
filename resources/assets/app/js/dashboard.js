//== Class definition
var Dashboard = function() {
    var cleanForm = function (form , clear) {
        clear ? form.clearForm() : form.trigger("reset");
        form.validate().resetForm();
        form.find('.has-error').removeClass("has-error");
        form.find('.has-success').removeClass("has-success");
        form.find('.form-control-feedback').remove();
    };

    var showErrorMsg = function(type, msg, title) {
        var content = {};

        content.message = msg;

        if(title!=false) {
            content.title = title;
        }

        var notify = $.notify(content, {
            type: type,
            allow_dismiss: true,
            newest_on_top: false,
            mouse_over:  false,
            showProgressbar:  false,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'top',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animated bounce',
                exit: 'animated bounce'
            }
        });
    }

    var showFormError = function(form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');
        alert.find('span').html(msg);
    }

    var datatableLatestOrders = function() {
        if ($('#m_datatable_latest_orders').length === 0) {
            return;
        }

        window.datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/products/all',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false //change from true to false on both
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            search: {
                input: $('#dt_search'),
            },

            layout: {
                theme: 'default',
                class: '',
                scroll: false, //changed from true=>false
                //height: 380,
                footer: false
            },

            sortable: true,

            filterable: false,

            pagination: true,

            columns: [{
                field: "shopify_pid",
                title: "Product ID",
                sortable: 'asc',
                filterable: false,
                width: 180
            }, {
                field: "title",
                title: "Product Title",
                width: 400,
                responsive: {
                    visible: 'lg'
                },
            }, {
                field: "status",
                title: "Status",
                width: 140,
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        enabled: {
                            'title': 'Enabled',
                            'class': ' m-badge--success'
                        },
                        disabled: {
                            'title': 'Disabled',
                            'class': ' m-badge--metal'
                        }
                    };
                    return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                },
            }, {
                field: "Actions",
                title: "Actions",
                sortable: false,
                //overflow: 'visible',
                width: 140,
                template: function(row) {
                    $("body").undelegate('#initXmlModal_' + row.shopify_pid, "click");
                    $('body').on('click','#initXmlModal_' + row.shopify_pid, function () {
                        $('#xml_form').attr('action', '/products/'+row.shopify_pid+'/xml')
                        $('#xml_shopify_pid').text(row.shopify_pid);
                        $('#xml_conf').val(row.xml_conf);
                        $('#xml_modal').modal();
                    });

                    $("body").undelegate('#initFtpModal_' + row.shopify_pid, "click");
                    $('body').on('click', '#initFtpModal_' + row.shopify_pid,function () {
                        $('#ftp_form').attr('action', '/products/'+row.shopify_pid+'/ftp')
                        $('#ftp_product_name').text(row.title);
                        $('#ftp_shopify_pid').text(row.shopify_pid);
                        if(row.ftp_type==null) {
                            row.ftp_type = 'sftp';
                        }
                        $("input[name=ftp_type][value='"+ row.ftp_type +"']").prop("checked",true);

                        if(row.ftp_host!=null) {
                            $("#ftp_host").val(row.ftp_host);
                        }

                        if(row.ftp_user!=null) {
                            $("#ftp_user").val(row.ftp_user);
                        }

                        if(row.ftp_password!=null) {
                            $("#ftp_password").val(row.ftp_password);
                        }

                        $("#ftp_folder").val(row.ftp_folder);

                        $('#ftp_modal').modal();
                    });



                    var html = '\
                        <a href="javascript:;" id="initFtpModal_'+ row.shopify_pid +'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit FTP">\
                            <i class="la la-cloud-upload\n"></i>\
                        </a>\
                        <a href="javascript:;" id="initXmlModal_'+ row.shopify_pid +'" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="Edit XML">\
                            <i class="la la-code"></i>\
                        </a>\
                    ';


                    if (row.status === 'enabled') {
                        $("body").undelegate('#disable_product_' + row.shopify_pid, "click");
                        $('body').on('click', '#disable_product_' + row.shopify_pid, function (e) {
                            this.blur();
                            mApp.block('#datatable_parent', {
                                overlayColor: '#000000',
                                type: 'loader',
                                state: 'primary',
                                message: 'Processing...'
                            });

                            $.post('/products/'+ row.shopify_pid +'/disable')
                                .done(function (data) {
                                    if(data==='success') {
                                        showErrorMsg('warning', 'Product #'+row.shopify_pid, "Product disabled");
                                    }
                                })
                                .fail(function () {
                                    showErrorMsg('danger', 'Something went wrong!', false);
                                })
                                .always(function() {
                                    window.datatable.load().on('m-datatable--on-ajax-done', function () {
                                        mApp.unblock('#datatable_parent');
                                    });
                                });
                        });

                        html += '\
                        <a href="javascript:;" id="disable_product_'+row.shopify_pid+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Disable Product">\
                            <i class="la la-ban"></i>\
                        </a>';
                    } else {
                        $("body").undelegate('#enable_product_' + row.shopify_pid, "click");
                        if (row.xml_conf == null || row.ftp_host == null) {
                            $('body').on('click', '#enable_product_' + row.shopify_pid, function (e) {
                                this.blur();
                                showErrorMsg('danger', 'FTP/XML settings not set.', false);
                            });
                        } else {
                            $('body').on('click', '#enable_product_' + row.shopify_pid, function (e) {
                                this.blur();
                                mApp.block('#datatable_parent', {
                                    overlayColor: '#000000',
                                    type: 'loader',
                                    state: 'primary',
                                    message: 'Processing...'
                                });

                                $.post('/products/'+ row.shopify_pid +'/enable')
                                    .done(function (data) {
                                        if(data==='success') {
                                            showErrorMsg('success', 'Product #'+row.shopify_pid+'', 'Product enabled');
                                        }
                                    })
                                    .fail(function () {
                                        showErrorMsg('danger', 'Something went wrong!', false);
                                    })
                                    .always(function() {
                                        window.datatable.load().on('m-datatable--on-ajax-done', function () {
                                            mApp.unblock('#datatable_parent');
                                        });
                                    });
                            });
                        }
                        html += '\
                        <a href="javascript:;" id="enable_product_' + row.shopify_pid + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Enable Product">\
                            <i class="la la-play"></i>\
                        </a>';
                    }

                    return html
                },
            }]
        });


    }

    var handleFtpForm = function () {
        $('#ftp_modal').on('hide.bs.modal', function () {
            $('.m-alert--outline').remove();
            cleanForm($('#ftp_form'), false);
        });

        $('#submit_ftp').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $('#ftp_form');

            form.validate({
                rules: {
                    ftp_password: {
                        required: true
                    },
                    ftp_host: {
                        required: true,
                    },
                    ftp_user: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr('action'),
                success: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    $('#ftp_modal').modal('hide');
                    cleanForm(form, false);

                    window.datatable.load();

                    if(response==='success') {
                        showErrorMsg('success', 'FTP settings are saved!', false);
                    } else {
                        showErrorMsg('danger', 'Something went wrong!', false);
                    }
                },
                error: function (response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);

                    window.datatable.load();

                    if(response.hasOwnProperty('responseJSON')) {
                        if(response.responseJSON.hasOwnProperty('errors')) {
                            showFormError(form, 'danger', response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]]);
                        } else {
                            showFormError(form, 'danger', 'Something went wrong!');
                        }
                    }
                }
            });
        })
    }

    var handleXmlForm = function () {
        $('#xml_modal').on('hide.bs.modal', function () {
            $('.m-alert--outline').remove();
            cleanForm($('#xml_form'), false);
        });

        $('#submit_xml').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $('#xml_form');

            form.validate({
                rules: {
                    xml_conf: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: form.attr('action'),
                success: function(response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    $('#xml_modal').modal('hide');
                    cleanForm(form, false);
                    window.datatable.load();

                    if(response==='success') {
                        showErrorMsg('success', 'XML settings are saved!', false);
                    } else {
                        showErrorMsg('danger', 'Something went wrong!', false);
                    }
                },
                error: function (response, status, xhr, $form) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    cleanForm(form, false);

                    window.datatable.load();

                    if(response.hasOwnProperty('responseJSON')) {
                        if(response.responseJSON.hasOwnProperty('errors')) {
                            showFormError(form, 'danger', response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]]);
                        } else {
                            showFormError(form, 'danger', 'Something went wrong!');
                        }
                    }
                }
            });
        })
    }

    return {
        //== Init demos
        init: function() {
            // datatables
            datatableLatestOrders();
            handleFtpForm();
            handleXmlForm();
        }
    };
}();

//== Class initialization on page load
jQuery(document).ready(function() {
    Dashboard.init();
});