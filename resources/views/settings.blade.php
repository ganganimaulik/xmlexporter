@extends('layouts.app')

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader-search">
                <h2 class="m-subheader-search__title">
                    Account Settings
                    <span class="m-subheader-search__desc">
								Enter Shopify Api Settings Or Change Password
							</span>
                </h2>
            </div>
            <div class="m-content">
                <!--Begin::Main Portlet-->
                <div class="row">
                    <div class="col-xl-12">

                        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                        role="tablist">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" data-toggle="tab"
                                               href="#m_user_profile_tab_1" role="tab">
                                                <i class="flaticon-share m--hide"></i>
                                                Shopify Config
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab"
                                               href="#m_user_profile_tab_3" role="tab">
                                                Change Password
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab"
                                               href="#m_user_profile_tab_4" role="tab">
                                                Other Settings
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="m_user_profile_tab_1">
                                    <form id="shopify_config_form" method="post" action="/settings/api"
                                          class="m-form m-form--fit m-form--label-align-right">
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group m--margin-top-10 m--hide">
                                                <div class="alert m-alert m-alert--default" role="alert">
                                                    The example form below demonstrates common HTML form elements that
                                                    receive updated styles from Bootstrap with additional classes.
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-10 ml-auto">
                                                    <h3 class="m-form__section">
                                                        1. Store Details
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="store_name" class="col-2 col-form-label">
                                                    Store Name
                                                </label>
                                                <div class="col-7">
                                                    @if(Auth::user()->api_credential != null)
                                                        <input id="store_name" name="store_name"
                                                               class="form-control m-input" type="text"
                                                               value="{{ Auth::user()->api_credential->store_name }}">
                                                    @else
                                                        <input id="store_name" name="store_name"
                                                               class="form-control m-input" type="text">
                                                    @endif
                                                    <span class="m-form__help">
                                                        https://<strong>mystore</strong>.myshopify.com/ => mystore
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-10 ml-auto">
                                                    <h3 class="m-form__section">
                                                        2. API Keys
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="api_key" class="col-2 col-form-label">
                                                    API Key
                                                </label>
                                                <div class="col-7">
                                                    @if(Auth::user()->api_credential != null)
                                                    <input class="form-control m-input" type="text"
                                                           id="api_key" name="api_key"
                                                           value="{{ Auth::user()->api_credential->api_key }}">
                                                    @else
                                                        <input class="form-control m-input" type="text"
                                                               id="api_key" name="api_key">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="api_password" class="col-2 col-form-label">
                                                    API Password
                                                </label>
                                                <div class="col-7">
                                                    @if(Auth::user()->api_credential != null)
                                                    <input class="form-control m-input" type="text"
                                                           id="api_password" name="api_password"
                                                           value="{{ Auth::user()->api_credential->api_password }}">
                                                    @else
                                                    <input class="form-control m-input" type="text"
                                                               id="api_password" name="api_password">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="shared_secret" class="col-2 col-form-label">
                                                    Shared secret
                                                </label>
                                                <div class="col-7">
                                                    @if(Auth::user()->api_credential != null)
                                                    <input class="form-control m-input" type="text"
                                                           id="shared_secret" name="shared_secret"
                                                           value="{{ Auth::user()->api_credential->shared_secret }}">
                                                    @else
                                                    <input class="form-control m-input" type="text"
                                                           id="shared_secret" name="shared_secret">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-2"></div>
                                                    <div class="col-7">
                                                        <button type="submit" id="save_shopify_config"
                                                                class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                            Save changes
                                                        </button>
                                                        <button type="reset" onclick="this.blur();"
                                                                class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>



                                <div class="tab-pane" id="m_user_profile_tab_3">
                                    <form class="m-form m-form--fit m-form--label-align-right" method="post"
                                          action="/settings/change-password">
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group m--margin-top-10 m--hide">
                                                <div class="alert m-alert m-alert--default" role="alert">
                                                    The example form below demonstrates common HTML form elements that
                                                    receive updated styles from Bootstrap with additional classes.
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="oldpassword" class="col-2 col-form-label">
                                                    Old Password
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="password"
                                                           name="oldpassword" id="oldpassword" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="newpassword" class="col-2 col-form-label">
                                                    New Password
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="password"
                                                           name="newpassword" id="newpassword"
                                                           autocomplete="new-password">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="newpassword_confirmation" class="col-2 col-form-label">
                                                    Repeat New Password
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="password"
                                                           name="newpassword_confirmation" id="newpassword_confirmation"
                                                           autocomplete="new-password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-2"></div>
                                                    <div class="col-7">
                                                        <button id="change_password" type="submit"
                                                                class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                            Save password
                                                        </button>
                                                        <button type="reset" onclick="this.blur();"
                                                                class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="m_user_profile_tab_4">
                                    <form class="m-form m-form--fit m-form--label-align-right"
                                          method="post" action="/settings/other-settings">
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group m--margin-top-10 m--hide">
                                                <div class="alert m-alert m-alert--default" role="alert">
                                                    The example form below demonstrates common HTML form elements that
                                                    receive updated styles from Bootstrap with additional classes.
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="email_notifications" class="col-2 col-form-label">
                                                    Email Notifications
                                                </label>
                                                <div class="col-7">
                                                    <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                                        <label>
                                                            <input type="checkbox"
                                                                   {{ Auth::user()->send_emails == true ? 'checked="checked"' : '' }}
                                                                   id="email_notifications" name="email_notifications">
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="timezone" class="col-2 col-form-label">
                                                    Set Timezone
                                                </label>
                                                <span id="user_timezone" class="m--hide">{{ Auth::user()->timezone }}</span>
                                                <div class="col-7">
                                                    <select name="timezone" class="form-control m-select2"
                                                            id="timezone">
                                                        <option></option>
                                                        <option value="Etc/GMT+12">(GMT-12:00) International Date Line
                                                            West
                                                        </option>
                                                        <option value="Pacific/Midway">(GMT-11:00) Midway Island,
                                                            Samoa
                                                        </option>
                                                        <option value="Pacific/Honolulu">(GMT-10:00) Hawaii</option>
                                                        <option value="US/Alaska">(GMT-09:00) Alaska</option>
                                                        <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US
                                                            & Canada)
                                                        </option>
                                                        <option value="America/Tijuana">(GMT-08:00) Tijuana, Baja
                                                            California
                                                        </option>
                                                        <option value="US/Arizona">(GMT-07:00) Arizona</option>
                                                        <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz,
                                                            Mazatlan
                                                        </option>
                                                        <option value="US/Mountain">(GMT-07:00) Mountain Time (US &
                                                            Canada)
                                                        </option>
                                                        <option value="America/Managua">(GMT-06:00) Central America
                                                        </option>
                                                        <option value="US/Central">(GMT-06:00) Central Time (US &
                                                            Canada)
                                                        </option>
                                                        <option value="America/Mexico_City">(GMT-06:00) Guadalajara,
                                                            Mexico City, Monterrey
                                                        </option>
                                                        <option value="Canada/Saskatchewan">(GMT-06:00) Saskatchewan
                                                        </option>
                                                        <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito,
                                                            Rio Branco
                                                        </option>
                                                        <option value="US/Eastern">(GMT-05:00) Eastern Time (US &
                                                            Canada)
                                                        </option>
                                                        <option value="US/East-Indiana">(GMT-05:00) Indiana (East)
                                                        </option>
                                                        <option value="Canada/Atlantic">(GMT-04:00) Atlantic Time
                                                            (Canada)
                                                        </option>
                                                        <option value="America/Caracas">(GMT-04:00) Caracas, La Paz
                                                        </option>
                                                        <option value="America/Manaus">(GMT-04:00) Manaus</option>
                                                        <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                                        <option value="Canada/Newfoundland">(GMT-03:30) Newfoundland
                                                        </option>
                                                        <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                                        <option value="America/Argentina/Buenos_Aires">(GMT-03:00)
                                                            Buenos Aires, Georgetown
                                                        </option>
                                                        <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                                        <option value="America/Montevideo">(GMT-03:00) Montevideo
                                                        </option>
                                                        <option value="America/Noronha">(GMT-02:00) Mid-Atlantic
                                                        </option>
                                                        <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.
                                                        </option>
                                                        <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                                        <option value="Africa/Casablanca">(GMT+00:00) Casablanca,
                                                            Monrovia, Reykjavik
                                                        </option>
                                                        <option value="Etc/Greenwich">(GMT+00:00) Greenwich Mean Time :
                                                            Dublin, Edinburgh, Lisbon, London
                                                        </option>
                                                        <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin,
                                                            Bern, Rome, Stockholm, Vienna
                                                        </option>
                                                        <option value="Europe/Belgrade">(GMT+01:00) Belgrade,
                                                            Bratislava, Budapest, Ljubljana, Prague
                                                        </option>
                                                        <option value="Europe/Brussels">(GMT+01:00) Brussels,
                                                            Copenhagen, Madrid, Paris
                                                        </option>
                                                        <option value="Europe/Sarajevo">(GMT+01:00) Sarajevo, Skopje,
                                                            Warsaw, Zagreb
                                                        </option>
                                                        <option value="Africa/Lagos">(GMT+01:00) West Central Africa
                                                        </option>
                                                        <option value="Asia/Amman">(GMT+02:00) Amman</option>
                                                        <option value="Europe/Athens">(GMT+02:00) Athens, Bucharest,
                                                            Istanbul
                                                        </option>
                                                        <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                                        <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                                        <option value="Africa/Harare">(GMT+02:00) Harare, Pretoria
                                                        </option>
                                                        <option value="Europe/Helsinki">(GMT+02:00) Helsinki, Kyiv,
                                                            Riga, Sofia, Tallinn, Vilnius
                                                        </option>
                                                        <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                                        <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                                        <option value="Africa/Windhoek">(GMT+02:00) Windhoek</option>
                                                        <option value="Asia/Kuwait">(GMT+03:00) Kuwait, Riyadh,
                                                            Baghdad
                                                        </option>
                                                        <option value="Europe/Moscow">(GMT+03:00) Moscow, St.
                                                            Petersburg, Volgograd
                                                        </option>
                                                        <option value="Africa/Nairobi">(GMT+03:00) Nairobi</option>
                                                        <option value="Asia/Tbilisi">(GMT+03:00) Tbilisi</option>
                                                        <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                                        <option value="Asia/Muscat">(GMT+04:00) Abu Dhabi, Muscat
                                                        </option>
                                                        <option value="Asia/Baku">(GMT+04:00) Baku</option>
                                                        <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                                        <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                                        <option value="Asia/Yekaterinburg">(GMT+05:00) Yekaterinburg
                                                        </option>
                                                        <option value="Asia/Karachi">(GMT+05:00) Islamabad, Karachi,
                                                            Tashkent
                                                        </option>
                                                        <option value="Asia/Calcutta">(GMT+05:30) Chennai, Kolkata,
                                                            Mumbai, New Delhi
                                                        </option>
                                                        <option value="Asia/Calcutta">(GMT+05:30) Sri Jayawardenapura
                                                        </option>
                                                        <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                                        <option value="Asia/Almaty">(GMT+06:00) Almaty, Novosibirsk
                                                        </option>
                                                        <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                                        <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)
                                                        </option>
                                                        <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi,
                                                            Jakarta
                                                        </option>
                                                        <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk
                                                        </option>
                                                        <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing,
                                                            Hong Kong, Urumqi
                                                        </option>
                                                        <option value="Asia/Kuala_Lumpur">(GMT+08:00) Kuala Lumpur,
                                                            Singapore
                                                        </option>
                                                        <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar
                                                        </option>
                                                        <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                                        <option value="Asia/Taipei">(GMT+08:00) Taipei</option>
                                                        <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo
                                                        </option>
                                                        <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                                        <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                                        <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                                        <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                                        <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                                        <option value="Australia/Canberra">(GMT+10:00) Canberra,
                                                            Melbourne, Sydney
                                                        </option>
                                                        <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                                        <option value="Pacific/Guam">(GMT+10:00) Guam, Port Moresby
                                                        </option>
                                                        <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok
                                                        </option>
                                                        <option value="Asia/Magadan">(GMT+11:00) Magadan, Solomon Is.,
                                                            New Caledonia
                                                        </option>
                                                        <option value="Pacific/Auckland">(GMT+12:00) Auckland,
                                                            Wellington
                                                        </option>
                                                        <option value="Pacific/Fiji">(GMT+12:00) Fiji, Kamchatka,
                                                            Marshall Is.
                                                        </option>
                                                        <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-2"></div>
                                                    <div class="col-7">
                                                        <button type="submit" id="other_settings"
                                                                class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                            Save changes
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End::Main Portlet-->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/exporter/js/settings.js"></script>
@endsection