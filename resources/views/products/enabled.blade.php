@extends('layouts.app')

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader-search">
                <h2 class="m-subheader-search__title">
                    Enabled Products
                    <span class="m-subheader-search__desc">
                        Enabled products for sending ftp orders
                    </span>
                </h2>
            </div>
            <div class="m-content">
                <!--Begin::Main Portlet-->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet m-portlet--mobile ">
                            <div class="m-portlet__body" id="datatable_parent">
                                <!--begin: Datatable -->
                                <div class="m_datatable" id="m_datatable_latest_orders"></div>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>

                </div>
                <!--End::Main Portlet-->

                <!--begin::Modal-->
                <div class="modal fade" id="xml_modal" tabindex="-1" role="dialog" aria-labelledby="xmlConfig" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    XML Config - Product #<span id="xml_shopify_pid"></span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" id="xml_form">
                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">
                                            XML:
                                        </label>
                                        <textarea class="form-control" id="xml_conf" name="xml_conf" rows="15"
                                                  style="resize: none;" data-role="none"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Cancel
                                </button>
                                <button type="button" class="btn btn-primary" id="submit_xml">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Modal-->

                <!--begin::Modal-->
                <div class="modal fade" id="ftp_modal" tabindex="-1" role="dialog" aria-labelledby="ftpConfig" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                    FTP Config - Product #<span id="ftp_shopify_pid"></span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="360">
                                    <form id="ftp_form" method="post">
                                        <div class="form-group">
                                            <label for="message-text" class="form-control-label">
                                                Product:
                                            </label>
                                            <p class="form-control-static" id="ftp_product_name">

                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="form-control-label">
                                                Type:
                                            </label>
                                            <div class=m-radio-inline">
                                                <label class="m-radio">
                                                    <input type="radio" name="ftp_type" value="sftp" checked>
                                                    SFTP
                                                    <span></span>
                                                </label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <label class="m-radio">
                                                    <input type="radio" name="ftp_type" value="ftp">
                                                    FTP
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">
                                                    Host:
                                                </label>
                                                <input type="text" name="ftp_host" class="form-control" id="ftp_host">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">
                                                    User:
                                                </label>
                                                <input type="text" name="ftp_user" class="form-control" id="ftp_user">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">
                                                    Password:
                                                </label>
                                                <input type="text" name="ftp_password" class="form-control" id="ftp_password">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">
                                                    Folder:
                                                </label>
                                                <input type="text" name="ftp_folder" class="form-control" id="ftp_folder">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary" id="submit_ftp">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Modal-->

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/assets/app/js/enabled.js" type="text/javascript"></script>
@endsection
